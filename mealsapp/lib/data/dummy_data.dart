import 'package:flutter/material.dart';
import 'package:mealsapp/models/category.dart';
import 'package:mealsapp/models/meals.dart';

const availableCategories = [
  Category1(
    id: 'c1',
    title: 'Italian',
    color: Colors.purple,
  ),
  Category1(
    id: 'c2',
    title: 'Quick & Easy',
    color: Colors.red,
  ),
  Category1(
    id: 'c3',
    title: 'Hamburgers',
    color: Colors.orange,
  ),
  Category1(
    id: 'c4',
    title: 'German',
    color: Colors.amber,
  ),
  Category1(
    id: 'c5',
    title: 'Light & Lovely',
    color: Colors.blue,
  ),
  Category1(
    id: 'c6',
    title: 'Exotic',
    color: Colors.green,
  ),
  Category1(
    id: 'c7',
    title: 'Breakfast',
    color: Colors.lightBlue,
  ),
  Category1(
    id: 'c8',
    title: 'Asian',
    color: Colors.lightGreen,
  ),
  Category1(
    id: 'c9',
    title: 'French',
    color: Colors.lightBlue,
  ),
];
const dummyMeals = [
  Meal(
      id: 'm1',
      categories: ['c1', 'c2'],
      title: 'Spagethi with Tomato Sauce',
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Spaghettata.JPG/269px-Spaghettata.JPG',
      ingredients: [
        '4 tomates',
        '1 Tablespoons of Olive Oil',
        '1 Onion',
        '250g Spaghetti',
        'Spices',
        'Cheese (optional)'
      ],
      steps: [
        'Cut the tomatoes and the union into small pieces.',
        'Boil some water - add salt to it once it boils.'
      ],
      duration: 20,
      complexity: Complexity.simple,
      affordability: Affordability.affordable,
      isGlutenFree: false,
      isLactoseFree: false,
      isVegan: false,
      isVegetarian: false),
  Meal(
      id: 'm2',
      categories: ['c2'],
      title: 'Spagethi with Tomato Sauce',
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Spaghettata.JPG/269px-Spaghettata.JPG',
      ingredients: [
        '4 tomates',
        '1 Tablespoons of Olive Oil',
        '1 Onion',
        '250g Spaghetti',
        'Spices',
        'Cheese (optional)'
      ],
      steps: [
        'Cut the tomatoes and the union into small pieces.',
        'Boil some water - add salt to it once it boils.'
      ],
      duration: 20,
      complexity: Complexity.simple,
      affordability: Affordability.affordable,
      isGlutenFree: false,
      isLactoseFree: false,
      isVegan: false,
      isVegetarian: false),
  Meal(
      id: 'm3',
      categories: ['c2', 'c3'],
      title: 'Classic Hamburger',
      imageUrl:
          'https://cdn.pixabay.com/photo/2014/10/23/18/05/burger-500054_1280.jpg',
      ingredients: [
        '300g Cattle Hack',
        '1 Tomato',
        '1 Cucumber',
        '1 Onion',
        'Ketchup',
        '2 Burger Buns'
      ],
      steps: [
        'Form 2 patties',
        'Boil some water - add salt to it once it boils.'
      ],
      duration: 45,
      complexity: Complexity.simple,
      affordability: Affordability.pricey,
      isGlutenFree: false,
      isLactoseFree: false,
      isVegan: false,
      isVegetarian: false)
];
