//import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mealsapp/data/dummy_data.dart';
import 'package:mealsapp/models/category.dart';
import 'package:mealsapp/screens/meals.dart';
import 'package:mealsapp/widgets/category_grid_item.dart';

class CategoriesScreen extends StatelessWidget {
  const CategoriesScreen({super.key});

//function untuk mengirim context ke class mealsscreen
  void _selectCategory(BuildContext context, Category1 category) {
    final filteredMeals = dummyMeals
        .where((meal) => meal.categories.contains(category.id))
        .toList();
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (ctx) => MealsScreen(
          title: category.title,
          meals: filteredMeals,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pick your category'),
      ),
      body: GridView(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20),
        children: [
          // Text('1', style: TextStyle(color: Colors.white)),
          // Text('2', style: TextStyle(color: Colors.white)),
          // Text('3', style: TextStyle(color: Colors.white)),
          // Text('4', style: TextStyle(color: Colors.white)),
          // Text('5', style: TextStyle(color: Colors.white)),
          // Text('6', style: TextStyle(color: Colors.white))
          for (final category in availableCategories)
            CategoryGridItem(
              category: category,
              onSelectedCategory: () {
                _selectCategory(context, category);
              },
            )

          //or alternative ( availableCategories.map((category)=>CategoryGridItem(category:)) )
        ],
      ),
    );
  }
}
